package main

import (
	"context"
	"database/sql"
	"log"
	"time"

	"github.com/adshao/go-binance/v2"
	_ "github.com/lib/pq"
)

type TickerData struct {
	Symbol  string
	Price   string
	AddedAt time.Time
}

// api - CKbKRM7L9FcUK6rFvqfcliHvOAEonXLE3oWlW3efQi0GWdCyIZokuUz7vo7dArrK
// secretKey - H5aDJgL5Up8DXS59WmSAbl6DEQVo0lm0GDWRiYF1BJb06eO9ejpkFuV5s4qmSV7i

func main() {
	apiKey := "YOUR_API_KEY"
	secretKey := "YOUR_SECRET_KEY"

	client := binance.NewClient(apiKey, secretKey)

	ticker := time.NewTicker(1 * time.Second) // Обновление цены каждые 5 секунд

	// Подключение к бд
	db, err := sql.Open("postgres", "user=postgres password=qwerty dbname=postgres host=postgres port=5432 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	for range ticker.C {
		price, err := client.NewListPricesService().Symbol("BTCUSDT").Do(context.Background())
		if err != nil {
			log.Println("Error fetching price:", err)
			continue
		}

		tickerData := TickerData{
			Symbol:  "BTCUSDT",
			Price:   price[0].Price,
			AddedAt: time.Now(),
		}

		// Сохранение данных в бд
		_, err = db.Exec("INSERT INTO ticker_data (symbol, price) VALUES ($1, $2)", tickerData.Symbol, tickerData.Price)
		if err != nil {
			log.Println("Error saving data to the database:", err)
			continue
		}
		log.Printf("Current BTC price: %s (added at: %s)\n", price[0].Price, tickerData.AddedAt.Format("2006-01-02 15:04:05"))
	}
}
